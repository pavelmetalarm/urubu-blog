FROM python:3.9.5-buster

RUN  apt-get update -y \
    && apt-get upgrade -y && apt-get install -y nginx tree \
    && rm -rf /var/lib/apt/lists/* && python -m pip install --upgrade 'markdown < 3' && python -m pip install urubu

EXPOSE 80

RUN rm /etc/nginx/sites-available/default && rm /etc/nginx/sites-enabled/default
COPY urubu-site.conf /etc/nginx/conf.d
WORKDIR /blog
ADD . /blog
ENTRYPOINT ["/bin/bash","entrypoint.sh"]  

